/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import QtMultimedia 5.12 as Multimedia
import org.kde.kcoreaddons 1.0 as KCoreAddons
import org.kde.kine 1.0

Item {
	required property var player
	anchors.fill: parent
	Rectangle {
		height: parent.children[1].height * 3
		anchors {
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}

		gradient: Gradient {
			GradientStop { position: 0.0; color: Qt.rgba(18/255, 18/255, 18/255, 0.0) }
			GradientStop { position: 1.0; color: Qt.rgba(18/255, 18/255, 18/255, 0.8) }
		}
	}
	ColumnLayout {
		visible: player.error == Multimedia.MediaPlayer.NoError
		anchors {
			bottom: parent.bottom
			horizontalCenter: parent.horizontalCenter
		}
		Kirigami.Theme.textColor: Qt.rgba(1,1,1,0.8)
		RowLayout {
			Layout.fillWidth: true
			Item {Layout.fillWidth: true}
			Kirigami.Heading {
				level: 4
				text: KCoreAddons.Format.formatDuration(player.position, 0)
			}
			QQC2.Slider {
				visible: player.seekable
				Layout.preferredWidth: Kirigami.Units.gridUnit*40
				from: 0
				to: player.duration || 1
				onMoved: {
					player.seek(value)
				}
				Timer {
					interval: 100
					running: true
					repeat: true
					onTriggered: {
						if (!parent.pressed) {
							parent.value = player.position || 0
						}
					}
				}
			}
			Kirigami.Heading {
				level: 4
				text: "-" + KCoreAddons.Format.formatDuration(player.duration - player.position / 1000, 0)
				Timer {
					interval: 100; running: true; repeat: true
					onTriggered: parent.text = "-" + KCoreAddons.Format.formatDuration(player.duration - player.position, 0)
				}
			}
			Item {Layout.fillWidth: true}
		}
		RowLayout {
			Layout.fillWidth: true
			Item {Layout.fillWidth: true}
			Row {
				Layout.alignment: Qt.AlignHCenter
				spacing: Kirigami.Units.largeSpacing
				QQC2.ToolButton {
					width: Kirigami.Units.iconSizes.large
					height: width
					icon.name: "media-seek-backward"

					TabIndicator {}

					onClicked: if (player.position > 5000) {
						player.seek(player.position - 5000)
					} else {
						player.seek(0)
					}
				}
				QQC2.ToolButton {
					width: Kirigami.Units.iconSizes.large
					height: width

					property bool paused: player.playbackState == Multimedia.MediaPlayer.PausedState
					icon.name: paused ? "media-playback-start" : "media-playback-pause"

					TabIndicator {}

					onClicked: paused ? player.play() : player.pause()
				}
				QQC2.ToolButton {
					width: Kirigami.Units.iconSizes.large
					height: width
					icon.name: "media-seek-forward"

					TabIndicator {}

					onClicked: if (player.position + 5000 < player.duration) {
						player.seek(player.position + 5000)
					} else {
						player.seek(player.duration)
					}
				}
			}
			Item {Layout.fillWidth: true}
		}
		Item { height: Kirigami.Units.largeSpacing }
	}
}
