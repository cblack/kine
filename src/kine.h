/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <QObject>
#include <QStandardPaths>
#include <QUrl>

#include <KLocalizedString>

#include <playlistmodel.h>

class Kine : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QUrl video MEMBER m_video NOTIFY videoChanged)
	Q_PROPERTY(QString videoFile READ videoFile NOTIFY videoChanged)
	Q_PROPERTY(PlaylistModel *playlist MEMBER m_playlist NOTIFY playlistChanged)
	Q_PROPERTY(bool shouldShowControls MEMBER m_shouldShowControls NOTIFY shouldShowControlsChanged)

public:
	Kine(QObject *parent = nullptr)
		: QObject(parent)
		, m_video(QUrl())
		, m_shouldShowControls(false)
		, m_playlist(new PlaylistModel(this))
	{
	}

private:
	auto videoFile() -> QString
	{
		return m_video.fileName();
	}

	QUrl m_video;
	bool m_shouldShowControls;
	PlaylistModel *m_playlist;

Q_SIGNALS:
	void videoChanged();
	void shouldShowControlsChanged();
	void playlistChanged();
};
