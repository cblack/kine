/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10
import QtQuick.Layouts 1.10
import QtQuick.Window 2.15
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import org.kde.kine 1.0

Item {
	required property string title
	signal openDrawer()

	anchors.fill: parent
	Rectangle {
		height: parent.children[1].height * 3
		anchors {
			left: parent.left
			right: parent.right
			top: parent.top
		}

		gradient: Gradient {
			GradientStop { position: 1.0; color: Qt.rgba(18/255, 18/255, 18/255, 0.0) }
			GradientStop { position: 0.0; color: Qt.rgba(18/255, 18/255, 18/255, 0.8) }
		}
	}
	ColumnLayout {
		anchors {
			top: parent.top
			left: parent.left
			leftMargin: Kirigami.Units.largeSpacing
			right: parent.right
			rightMargin: Kirigami.Units.largeSpacing
		}
		Item { height: Kirigami.Units.largeSpacing }
		Item {
			Layout.fillWidth: true
			implicitHeight: childrenRect.height
			QQC2.ToolButton {
				anchors.left: parent.left

				icon.name:
					Qt.application.layoutDirection === Qt.RightToLeft
					? "arrow-left"
					: "arrow-right"

				onClicked: openDrawer()

				TabIndicator {}
			}
			Kirigami.Heading {
				anchors.horizontalCenter: parent.horizontalCenter
				text: title || Kine.playlist.videoFile
				level: 3
			}
			QQC2.ToolButton {
				anchors.right: parent.right
				text: applicationWindow().visibility != Window.FullScreen ?
					  i18n("Fullscreen") : i18n("Restore")

				icon.name: "view-fullscreen"

				onClicked: applicationWindow().visibility != Window.FullScreen ?
						   applicationWindow().showFullScreen() : applicationWindow().showNormal()

				TabIndicator {}
			}
		}
	}
}
