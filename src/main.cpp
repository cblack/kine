/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include <QIcon>
#include <QModelIndex>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <KLocalizedContext>
#include <KLocalizedString>

#include "kine.h"
#include "mpris.h"

auto main(int argc, char *argv[]) -> int
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	auto app = new QApplication(argc, argv);

	{
		QString mpris2Name = QStringLiteral("org.mpris.MediaPlayer2.org.kde.Kine");
		bool canRegister = QDBusConnection::sessionBus().registerService(mpris2Name);

		if (!canRegister) {
			canRegister = QDBusConnection::sessionBus().registerService(QLatin1String("org.mpris.MediaPlayer2.org.kde.Kine.instance") % QString::number(qApp->applicationPid()));
		}

		new MediaPlayer2Adaptor(app);
		QDBusConnection::sessionBus().registerObject(QLatin1String("/org/mpris/MediaPlayer2"), app);
	}

	KLocalizedString::setApplicationDomain("org.kde.Kine");

	QApplication::setOrganizationName(QStringLiteral("KDE"));
	QApplication::setOrganizationDomain(QStringLiteral("org.kde"));
	QApplication::setApplicationName(QStringLiteral("Kine"));

	QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.kde.Kine")));
	QApplication::setDesktopFileName(QStringLiteral("org.kde.Kine.desktop"));

	Kine kine;
	qmlRegisterSingletonInstance("org.kde.kine", 1, 0, "Kine", &kine);

	QQmlApplicationEngine engine;
	engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(
		&engine,
		&QQmlApplicationEngine::objectCreated,
		app,
		[url](QObject *obj, const QUrl &objUrl) {
			if ((obj == nullptr) && url == objUrl) {
				QCoreApplication::exit(-1);
			}
		},
		Qt::QueuedConnection);
	engine.load(url);

	return QApplication::exec();
}
