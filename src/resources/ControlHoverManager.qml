/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10
import org.kde.kine 1.0

Item {
	property bool showControls: true
	anchors.fill: parent
	function tabbed() {
		showControls = true
		mouseArea.inactive.restart()
	}
	MouseArea {
		id: mouseArea

		property Timer exit: Timer {
			interval: 400
			onTriggered: showControls = false
		}

		property Timer inactive: Timer {
			interval: 5000
			onTriggered: showControls = false
		}

		anchors.fill: parent
		hoverEnabled: true
		propagateComposedEvents: true

		onEntered: {
			showControls = true
			exit.stop()
			inactive.restart()
		}
		onPositionChanged: {
			showControls = true
			inactive.restart()
		}
		onExited: {
			exit.restart()
			inactive.stop()
		}
	}
}
