import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Dialogs 1.0
import org.kde.kine 1.0

Kirigami.GlobalDrawer {
	implicitWidth: Math.min(Math.max(0.33 * applicationWindow().width, Kirigami.Units.gridUnit * 15), applicationWindow().width)
	height: parent.height
	leftPadding: 0
	rightPadding: 0
	topPadding: 0
	bottomPadding: 0

	FileDialog {
		id: fileDialog
		folder: shortcuts.movies
		onAccepted: {
			Kine.playlist.enqueue(fileUrl)
		}
	}

	ColumnLayout {
		spacing: 0
		QQC2.ScrollView {
			Layout.fillHeight: true
			Layout.fillWidth: true
			ListView {
				model: Kine.playlist

				delegate: Kirigami.BasicListItem {
					required property string display
					required property int index
					bold: index == Kine.playlist.playing
					text: display
					action: Kirigami.Action {
						onTriggered: Kine.playlist.playing = index
					}
				}

				Kirigami.PlaceholderMessage {
					text: i18n("You don't have any videos in your playlist. Why not play some?")

					visible: parent.count === 0

					anchors.centerIn: parent
					width: parent.width - (Kirigami.Units.largeSpacing * 4)
				}
			}
		}

		QQC2.ToolBar {
			position: QQC2.ToolBar.Footer
			Layout.fillWidth: true

			QQC2.ToolButton {
				text: i18n("Open Video...")
				icon.name: "document-open"

				anchors {
					right: parent.right
				}

				onClicked: fileDialog.open()
			}
		}
	}
}
