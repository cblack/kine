/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import org.kde.kirigami 2.12 as Kirigami

Kirigami.RouterWindow {
	property Drawer drawer: Drawer {}

	pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None
	controlsVisible: false
	initialRoute: "home"

	Kirigami.PageRoute {
		name: "home"
		HomePage {}
	}

	Kirigami.PageRoute {
		name: "video"
		PlayerPage {}
	}
}
