#pragma once

#include <QAbstractListModel>
#include <QDebug>
#include <QUrl>

class PlaylistModel : public QAbstractListModel
{
	Q_OBJECT

	Q_PROPERTY(bool canNext READ canNext NOTIFY playingChanged)
	Q_PROPERTY(bool canPrevious READ canPrevious NOTIFY playingChanged)
	Q_PROPERTY(int playing READ playing WRITE setPlaying NOTIFY playingChanged)
	Q_PROPERTY(QUrl video READ video NOTIFY playingChanged)
	Q_PROPERTY(QString videoFile READ videoFile NOTIFY playingChanged)

public:
	PlaylistModel(QObject *parent)
		: QAbstractListModel(parent)
	{
	}

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;

	Q_INVOKABLE void enqueue(const QUrl &url);
	Q_INVOKABLE void playNext();
	Q_INVOKABLE void playPrevious();

	int playing() const;
	void setPlaying(int idx);

	bool canNext();
	bool canPrevious();

	QUrl video() const;
	QString videoFile() const;

Q_SIGNALS:
	void playingChanged();
	void nextChanged();
	void previousChanged();

private:
	QList<QUrl> m_tracks;
	int m_currentlyPlaying = -1;
};
