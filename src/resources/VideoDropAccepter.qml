/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10
import org.kde.kirigami 2.12 as Kirigami
import org.kde.kine 1.0

DropArea {
	anchors.fill: parent
	onDropped: function(ev) {
		if (ev.hasUrls) {
			Kine.playlist.enqueue(ev.urls[0])
		}
	}
	Rectangle {
		color: Qt.rgba(0.07, 0.07, 0.07, 0.8)
		anchors.fill: parent
		opacity: parent.containsDrag ? 1 : 0
		Behavior on opacity {
			NumberAnimation {
				duration: 200
				easing.type: Easing.InOutQuad
			}
		}
		Kirigami.Icon {
			Kirigami.Theme.textColor: "white"
			anchors.centerIn: parent
			source: "document-open"
			width: Math.min(parent.parent.width / 2, Kirigami.Units.gridUnit * 15)
			height: width
		}
	}
}
