#include "playlistmodel.h"

int PlaylistModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)

	return m_tracks.count();
}

QVariant PlaylistModel::data(const QModelIndex &idx, int role) const
{
	if (!checkIndex(idx)) {
		return QVariant();
	}

	auto row = idx.row();

	switch (role) {
	case Qt::DisplayRole:
		return m_tracks[row].fileName();
	}

	return QVariant();
}

void PlaylistModel::enqueue(const QUrl &url)
{
	beginInsertRows(QModelIndex(), m_tracks.count(), m_tracks.count());
	m_tracks << url;
	endInsertRows();
	Q_EMIT canNext();
	if (m_currentlyPlaying == -1) {
		m_currentlyPlaying = 0;
		Q_EMIT playingChanged();
	}
}

int PlaylistModel::playing() const
{
	return m_currentlyPlaying;
}

void PlaylistModel::setPlaying(int currentlyPlaying)
{
	if (m_currentlyPlaying == currentlyPlaying) {
		return;
	}
	m_currentlyPlaying = currentlyPlaying;
	Q_EMIT playingChanged();
}

bool PlaylistModel::canNext()
{
	return m_currentlyPlaying != (m_tracks.length() - 1);
}

bool PlaylistModel::canPrevious()
{
	return m_currentlyPlaying > 0;
}

QUrl PlaylistModel::video() const
{
	if (m_currentlyPlaying == -1) {
		return QUrl();
	}
	return m_tracks[m_currentlyPlaying];
}

QString PlaylistModel::videoFile() const
{
	if (m_currentlyPlaying == -1) {
		return QString();
	}
	return m_tracks[m_currentlyPlaying].fileName();
}

void PlaylistModel::playNext()
{
	if (!canNext()) {
		return;
	}

	m_currentlyPlaying++;
	Q_EMIT playingChanged();
}

void PlaylistModel::playPrevious()
{
	if (!canPrevious()) {
		return;
	}

	m_currentlyPlaying--;
	Q_EMIT playingChanged();
}
