/*
 *  SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls 2.14 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
import org.kde.kine 1.0

Kirigami.Page {
	id: page

	signal openVideoPage()

	onOpenVideoPage: Kirigami.PageRouter.navigateToRoute("video")

	FileDialog {
		id: fileDialog
		folder: shortcuts.movies
		onAccepted: {
			Kine.playlist.enqueue(fileUrl)
			page.openVideoPage()
		}
	}
	ColumnLayout {
		anchors.centerIn: parent
		Kirigami.Icon {
			source: "org.kde.Kine"

			Layout.preferredWidth: Kirigami.Units.gridUnit * 6
			Layout.preferredHeight: Layout.preferredWidth
			Layout.alignment: Qt.AlignHCenter
		}
		Kirigami.Heading {
			text: i18n("Welcome to Kine")

			horizontalAlignment: Text.AlignHCenter
			Layout.fillWidth: true
		}
		QQC2.Button {
			text: i18n("Open File")
			icon.name: "document-open"
			onClicked: fileDialog.open()

			Layout.alignment: Qt.AlignHCenter
		}
		QQC2.Button {
			id: streamButton
			text: i18n("Stream Video From URL")
			icon.name: "document-open"

			Layout.alignment: Qt.AlignHCenter
			onClicked: {
				buttonShow.visible = true
				this.visible = false
			}
		}
		RowLayout {
			id: buttonShow
			visible: false

			Layout.alignment: Qt.AlignHCenter

			QQC2.TextField {
				id: streamTextField

				Layout.alignment: Qt.AlignHCenter

				onAccepted: {
					Kine.playlist.enqueue(text)
					page.openVideoPage()
				}
				Keys.onEscapePressed: {
					buttonShow.visible = false
					streamButton.visible = true
				}
			}
			QQC2.Button {
				icon.name: Qt.application.layoutDirection == Qt.RightToLeft ? "arrow-left" : "arrow-right"
				onClicked: streamTextField.accepted()
			}
		}
	}
}
