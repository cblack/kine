/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import org.kde.kirigami 2.12 as Kirigami
import QtMultimedia 5.12 as Multimedia
import org.kde.kine 1.0

Kirigami.Page {
	id: page
	property bool showControls: true

	topPadding: 0
	leftPadding: 0
	rightPadding: 0
	bottomPadding: 0

	Rectangle {
		anchors.fill: parent
		color: "black"
	}

	Multimedia.VideoOutput {
		anchors.fill: parent
		source: Multimedia.MediaPlayer {
			id: videoPlayer
			autoPlay: true
			source: Kine.playlist.video
			onPlaybackStateChanged: {
				if (position === duration && playbackState === 0) {
					if (Kine.playlist.canNext) {
						Kine.playlist.playNext()
					}
				}
			}
		}
	}

	ControlHoverManager {
		onShowControlsChanged: page.showControls = showControls
	}
	FocusScope {
		anchors.fill: parent
		opacity: showControls ? 1 : 0
		Behavior on opacity {
			NumberAnimation {
				duration: 200
			}
		}
		VideoControls {
			player: videoPlayer
		}
		VideoData {
			title: videoPlayer.metaData.title ?? ""
			onOpenDrawer: {
				drawer.open()
				drawer.forceActiveFocus()
			}
		}
	}
	VideoDropAccepter {}
}
